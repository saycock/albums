json.array!(@artists) do |artist|
  json.extract! artist, :id, :name, :bio, :photo_url, :year_formed
  json.url artist_url(artist, format: :json)
end
