class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists do |t|
      t.string :name
      t.text :bio
      t.string :photo_url
      t.string :year_formed

      t.timestamps null: false
    end
  end
end
